module.exports = {
  diff: true,
  extension: ['ts'],
  opts: false,
  package: './package.json',
  reporter: 'spec',
  slow: 75,
  timeout: 2000,
  sort: true,
  require: ['ts-node/register'],
  ui: 'bdd',
  spec: ['test/**/*.spec.ts'],
  exit: true
}
