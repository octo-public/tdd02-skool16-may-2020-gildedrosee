import { expect }                    from 'chai'
import { GildedRose, Item }          from '../app/gilded-rose'
import { GildedRoseOld }             from '../app/gilded-rose_old'
import { updateFixtureForGivenDays } from './golden-master-text-test'

describe('Gilded Rose', function () {

	it('Golden master 1', function () {
		const expected = updateFixtureForGivenDays(10)

		expect(expected).to.eql([
			'+5 Dexterity Vest 10 20',
			'Aged Brie 2 0',
			'Elixir of the Mongoose 5 7',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 15 20',
			'Backstage passes to a TAFKAL80ETC concert 10 49',
			'Backstage passes to a TAFKAL80ETC concert 5 49',
			'Backstage passes to a TAFKAL80ETC concert 5 30',
			'+5 Dexterity Vest 9 19',
			'Aged Brie 1 1',
			'Elixir of the Mongoose 4 6',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 14 21',
			'Backstage passes to a TAFKAL80ETC concert 9 50',
			'Backstage passes to a TAFKAL80ETC concert 4 50',
			'Backstage passes to a TAFKAL80ETC concert 4 32',
			'+5 Dexterity Vest 8 18',
			'Aged Brie 0 2',
			'Elixir of the Mongoose 3 5',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 13 22',
			'Backstage passes to a TAFKAL80ETC concert 8 50',
			'Backstage passes to a TAFKAL80ETC concert 3 50',
			'Backstage passes to a TAFKAL80ETC concert 3 35',
			'+5 Dexterity Vest 7 17',
			'Aged Brie -1 4',
			'Elixir of the Mongoose 2 4',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 12 23',
			'Backstage passes to a TAFKAL80ETC concert 7 50',
			'Backstage passes to a TAFKAL80ETC concert 2 50',
			'Backstage passes to a TAFKAL80ETC concert 2 38',
			'+5 Dexterity Vest 6 16',
			'Aged Brie -2 6',
			'Elixir of the Mongoose 1 3',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 11 24',
			'Backstage passes to a TAFKAL80ETC concert 6 50',
			'Backstage passes to a TAFKAL80ETC concert 1 50',
			'Backstage passes to a TAFKAL80ETC concert 1 41',
			'+5 Dexterity Vest 5 15',
			'Aged Brie -3 8',
			'Elixir of the Mongoose 0 2',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 10 25',
			'Backstage passes to a TAFKAL80ETC concert 5 50',
			'Backstage passes to a TAFKAL80ETC concert 0 50',
			'Backstage passes to a TAFKAL80ETC concert 0 44',
			'+5 Dexterity Vest 4 14',
			'Aged Brie -4 10',
			'Elixir of the Mongoose -1 0',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 9 27',
			'Backstage passes to a TAFKAL80ETC concert 4 50',
			'Backstage passes to a TAFKAL80ETC concert -1 0',
			'Backstage passes to a TAFKAL80ETC concert -1 0',
			'+5 Dexterity Vest 3 13',
			'Aged Brie -5 12',
			'Elixir of the Mongoose -2 0',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 8 29',
			'Backstage passes to a TAFKAL80ETC concert 3 50',
			'Backstage passes to a TAFKAL80ETC concert -2 0',
			'Backstage passes to a TAFKAL80ETC concert -2 0',
			'+5 Dexterity Vest 2 12',
			'Aged Brie -6 14',
			'Elixir of the Mongoose -3 0',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 7 31',
			'Backstage passes to a TAFKAL80ETC concert 2 50',
			'Backstage passes to a TAFKAL80ETC concert -3 0',
			'Backstage passes to a TAFKAL80ETC concert -3 0',
			'+5 Dexterity Vest 1 11',
			'Aged Brie -7 16',
			'Elixir of the Mongoose -4 0',
			'Sulfuras, Hand of Ragnaros 0 80',
			'Sulfuras, Hand of Ragnaros -1 80',
			'Backstage passes to a TAFKAL80ETC concert 6 33',
			'Backstage passes to a TAFKAL80ETC concert 1 50',
			'Backstage passes to a TAFKAL80ETC concert -4 0',
			'Backstage passes to a TAFKAL80ETC concert -4 0'
		])
	})

	it('Golden master 2', () => {
		const items = [
			new Item('+5 Dexterity Vest', 10, 20), //
			new Item('Aged Brie', 2, 0), //
			new Item('Elixir of the Mongoose', 5, 7), //
			new Item('Sulfuras, Hand of Ragnaros', 0, 80), //
			new Item('Sulfuras, Hand of Ragnaros', -1, 80),
			new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
			new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
			new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
			new Item('Backstage passes to a TAFKAL80ETC concert', 5, 30),
		]
		const items2 = [
			new Item('+5 Dexterity Vest', 10, 20), //
			new Item('Aged Brie', 2, 0), //
			new Item('Elixir of the Mongoose', 5, 7), //
			new Item('Sulfuras, Hand of Ragnaros', 0, 80), //
			new Item('Sulfuras, Hand of Ragnaros', -1, 80),
			new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
			new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
			new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
			new Item('Backstage passes to a TAFKAL80ETC concert', 5, 30),
		]
		const gildedRose = new GildedRose(items)
		const gildedRoseOld = new GildedRoseOld(items2)

		for (let i = 0; i < 10; i++) {
			gildedRose.updateQuality()
			gildedRoseOld.updateQuality()

			gildedRose.items.forEach((item, index) => {
				expect(item).to.eql(gildedRoseOld.items[index])
			})
		}
	})

})
