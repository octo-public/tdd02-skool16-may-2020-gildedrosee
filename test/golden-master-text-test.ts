import { Item, GildedRose } from '../app/gilded-rose'

const items = [
	new Item('+5 Dexterity Vest', 10, 20), //
	new Item('Aged Brie', 2, 0), //
	new Item('Elixir of the Mongoose', 5, 7), //
	new Item('Sulfuras, Hand of Ragnaros', 0, 80), //
	new Item('Sulfuras, Hand of Ragnaros', -1, 80),
	new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
	new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
	new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
	new Item('Backstage passes to a TAFKAL80ETC concert', 5, 30),
]

export function updateFixtureForGivenDays(days: number) {
	const text: string[] = []
	const gildedRose     = new GildedRose(items)
	for (let i = 0; i < days; i++) {
		items.forEach(element => {
			text.push(element.name + ' ' + element.sellIn + ' ' + element.quality)
		})
		gildedRose.updateQuality()
	}
	return text
}
